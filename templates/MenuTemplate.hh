#ifndef menulib_hh
#define menulib_hh
{#
 # @author: Takashi MATSUSHITA
 #}
/* automatically generated from {{ menu.getName() }} with menu2lib.py */
/* https://gitlab.cern.ch/cms-l1t-utm/scripts */

#include <string>
#include <vector>
#include <functional>

#include "TTreeReaderValue.h"
#include "L1Trigger/L1TNtuples/interface/L1AnalysisL1UpgradeDataFormat.h"

// temporary hack
namespace L1Analysis
{
  const int kMissingEtHF = 8;
  const int kTotalEtxHF = 9;
  const int kTotalEtyHF = 10;
  const int kMinBiasHFP0 = 11;
  const int kMinBiasHFM0 = 12;
  const int kMinBiasHFP1 = 13;
  const int kMinBiasHFM1 = 14;
  const int kTotalEtHF = 15;
  const int kTotalEtEm = 16;
  const int kTotalHtHF = 17;
  const int kTotalHtxHF = 18;
  const int kTotalHtyHF = 19;
  const int kMissingHtHF = 20;
  const int kTowerCount = 21;
}

// utility methods
void
getCombination(int N,
               int K,
               std::vector<std::vector<int> >& combination);

void
getPermutation(int N,
               std::vector<std::vector<int> >& permutation);

const long long POW10[] =
{
                      1, 
                     10,
                    100,
                   1000,
                  10000,
                 100000,
                1000000,
               10000000,
              100000000,
             1000000000,
            10000000000,
           100000000000,
          1000000000000,
         10000000000000,
        100000000000000,
       1000000000000000,
      10000000000000000,
     100000000000000000
};


std::string getNameFromId(const int index);

int getIdFromName(const std::string& name);

typedef bool (*AlgorithmFunction)(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>&);

AlgorithmFunction getFuncFromId(const int index);

AlgorithmFunction getFuncFromName(const std::string& name);

bool addFuncFromName(std::map<std::string, std::function<bool()>> &L1SeedFun,
                     L1Analysis::L1AnalysisL1UpgradeDataFormat* ntuple_);

// algorithms
{% for name, algo in menu.getAlgorithmMapPtr().iteritems() %}
bool
{{ name }}(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data);
{% endfor %}


{#
 # jinja templates
 #}
{% set scaleMap = menu.getScaleMapPtr() %}

// generate scales
{% include 'Scales.jinja2' %}

// generate LUTs
{% include 'CaloCaloLUT.jinja2' %}
{% include 'CaloEsumLUT.jinja2' %}
{% include 'CaloMuonLUT.jinja2' %}
{% include 'MuonEsumLUT.jinja2' %}
{% include 'MuonMuonLUT.jinja2' %}

{# prefix #}
{% set prefixMuon = 'data->muon' %}
{% set prefixEg = 'data->eg' %}
{% set prefixTau = 'data->tau' %}
{% set prefixJet = 'data->jet' %}
{% set prefixSum = 'data->sum' %}
{% set maxTau = 8 %}

#endif // menulib_hh
// eof
