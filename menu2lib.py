#!/bin/env python
#
# @author: Takashi MATSUSHITA
#

import argparse
import math
import os
import re
import string
from jinja2 import Environment, FileSystemLoader

import tmGrammar
import tmEventSetup


#
# filters
#
def toDecimal(value, nBit):
  signed = 1 << (nBit - 1);
  return (~signed & value) - signed if (signed & value) else value


def toCharge(charge):
  rc = 0
  if charge == "positive": rc = 1
  elif charge == "negative": rc = -1
  return rc


def chkChgCor(cuts, prefix, nobjects):
  data = {}
  data['prefix'] = prefix;
  data['nobjects'] = nobjects;
  text = ""
  check = """
      // charge correlation
      bool equal = true;
      for (size_t mm = 0; mm < %(nobjects)s -1; mm++)
      {
        int idx0 = candidates.at(set.at(indicies.at(mm)));
        int idx1 = candidates.at(set.at(indicies.at(mm+1)));
        if (%(prefix)sChg.at(idx0) != %(prefix)sChg.at(idx1))
        {
          equal = false;
          break;
        }
      }
""" % data

  for cut in cuts:
    if cut.getCutType() == tmEventSetup.ChargeCorrelation:
      if cut.getData() == "os":
        if nobjects == 4:
          text = """
      size_t positives = 0;
      for (size_t mm = 0; mm < %(nobjects)s; mm++)
      {
        int idx = candidates.at(set.at(indicies.at(mm)));
        if (%(prefix)sChg.at(idx) > 0) positives++;
      }
      // charge correlation: "os"
      if (positives != 2) continue;  // as in emulator
""" % data
        else:
          text = check + """
      // charge correlation: "os"
      if (equal) continue;
"""
      elif cut.getData() == "ls":
        text = check + """
      // charge correlation: "ls"
      if (not equal) continue;
"""
  return text


def hasEtaPhiCuts(objects):
  nEtaCuts = 0
  nPhiCuts = 0

  for obj in objects:
    nEta = 0
    nPhi = 0
    for cut in obj.getCuts():
      if cut.getCutType() == tmEventSetup.Eta:
        nEta += 1
      elif cut.getCutType() == tmEventSetup.Phi:
        nPhi += 1
    nEtaCuts = max(nEtaCuts, nEta)
    nPhiCuts = max(nPhiCuts, nPhi)

  declaration = ""
  if nEtaCuts == 1:
    declaration += "bool etaWindow1;"
  elif nEtaCuts == 2:
    declaration += "bool etaWindow1, etaWindow2;"

  if nPhiCuts == 1:
    declaration += "bool phiWindow1;"
  elif nPhiCuts == 2:
    declaration += "bool phiWindow1, phiWindow2;"

  return declaration


def sortObjects(obj1, obj2):
  if obj1.getType() == obj2.getType(): return obj1, obj2

  if obj1.getType() == tmEventSetup.Muon:
    if obj2.getType() not in (tmEventSetup.ETM, tmEventSetup.HTM, tmEventSetup.ETMHF):
      return obj2, obj1

  elif obj1.getType() == tmEventSetup.Jet:
    if obj2.getType() not in (tmEventSetup.Tau, tmEventSetup.ETM, tmEventSetup.HTM,
                              tmEventSetup.ETMHF, tmEventSetup.Muon):
      return obj2, obj1

  elif obj1.getType() == tmEventSetup.Tau:
    if obj2.getType() not in (tmEventSetup.ETM, tmEventSetup.HTM, tmEventSetup.ETMHF,
                              tmEventSetup.Muon):
      return obj2, obj1
  elif obj1.getType() in (tmEventSetup.ETM, tmEventSetup.HTM, tmEventSetup.ETMHF):
    return obj2, obj1

  return obj1, obj2


def toCpp(value):
  tokens = value.split()
  array = []

  pattern = "(\W*[a-zA-Z0-9]+_[0-9]+)(\W*)"
  prog = re.compile(pattern)

  for token in tokens:
    result = prog.match(token)
    if result:
      array.append(result.group(1) + '(data)' + result.group(2))
    elif token == tmGrammar.AND:
      array.append(tmGrammar.AND.lower())
    elif token == tmGrammar.OR:
      array.append(tmGrammar.OR.lower())
    else:
      if tmGrammar.NOT in token:
        token = token.replace(tmGrammar.NOT, tmGrammar.NOT.lower())
      array.append(token)
  return ' '.join(array)


def getLut(scale, precision):
  lut = tmEventSetup.LlongVec()
  tmEventSetup.getLut(lut, scale, precision.getNbits())
  return lut


def getCaloMuonEtaConversionLut(calo, muon):
  lut = tmEventSetup.LlongVec()
  tmEventSetup.getCaloMuonEtaConversionLut(lut, calo, muon)
  return lut


def getCaloMuonPhiConversionLut(calo, muon):
  lut = tmEventSetup.LlongVec()
  tmEventSetup.getCaloMuonPhiConversionLut(lut, calo, muon)
  return lut


def getDeltaLut(scale1, scale2, precision):
  vec = tmEventSetup.DoubleVec()
  lut = tmEventSetup.LlongVec()
  n = tmEventSetup.getDeltaVector(vec, scale1, scale2)
  tmEventSetup.setLut(lut, vec, precision.getNbits())
  return lut


def getCosLut(scale1, scale2, precision):
  vec = tmEventSetup.DoubleVec()
  lut = tmEventSetup.LlongVec()
  n = tmEventSetup.getDeltaVector(vec, scale1, scale2)
  tmEventSetup.applyCos(vec, n)
  tmEventSetup.setLut(lut, vec, precision.getNbits())
  return lut


def getCoshLut(scale1, scale2, precision):
  vec = tmEventSetup.DoubleVec()
  lut = tmEventSetup.LlongVec()
  n = tmEventSetup.getDeltaVector(vec, scale1, scale2)
  tmEventSetup.applyCosh(vec, n)
  tmEventSetup.setLut(lut, vec, precision.getNbits())
  return lut


def toMass(value):
  return math.sqrt(2.*value)


def warning(message):
  print message
  return ''


THIS_DIR = os.path.dirname(os.path.abspath(__file__)) + "/templates"


def render(menu, template, name):
  j2_env = Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)

  j2_env.add_extension('jinja2.ext.loopcontrols')
  j2_env.filters['toDecimal'] = toDecimal
  j2_env.filters['toCharge'] = toCharge
  j2_env.filters['hasEtaPhiCuts'] = hasEtaPhiCuts
  j2_env.filters['sortObjects'] = sortObjects
  j2_env.filters['toCpp'] = toCpp
  j2_env.filters['getLut'] = getLut
  j2_env.filters['getDeltaLut'] = getDeltaLut
  j2_env.filters['getCosLut'] = getCosLut
  j2_env.filters['getCoshLut'] = getCoshLut
  j2_env.filters['getCaloMuonEtaConversionLut'] = getCaloMuonEtaConversionLut
  j2_env.filters['getCaloMuonPhiConversionLut'] = getCaloMuonPhiConversionLut
  j2_env.filters['warning'] = warning
  j2_env.filters['toMass'] = toMass
  j2_env.filters['chkChgCor'] = chkChgCor

  association = {
      "tmGrammar": tmGrammar,
      "tmEventSetup": tmEventSetup,
      "menu": menu
      }

  return j2_env.get_template(template).render(association)


if __name__ == '__main__':
  xml = 'L1Menu_Collisions2016_v5.xml'
  output = 'menulib.cc'

  parser = argparse.ArgumentParser()

  parser.add_argument("--menu", dest="xml", default=xml, type=str, action="store", required=True, help="path to the level1 trigger menu xml file")
  parser.add_argument("--output", dest="output", default=output, type=str, action="store", help="output c++ file name")
  parser.add_argument("--TTreeReader", dest="useTTreeReader", action="store_true", help="use TTreeReaderValue to access ntuple variables")
  parser.set_defaults(useTTreeReader = False)

  options = parser.parse_args()

  menu = tmEventSetup.getTriggerMenu(options.xml)

  name = os.path.basename(output)
  name, ext = os.path.splitext(name)

  text = render(menu, 'MenuTemplate.cc', name)
  if not options.useTTreeReader:
    text = string.replace(text, r"TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>&",
                                r"L1Analysis::L1AnalysisL1UpgradeDataFormat*")
  with open(options.output, "wb") as fp:
    fp.write(text)

  text = render(menu, 'MenuTemplate.hh', name)
  if not options.useTTreeReader:
    text = string.replace(text, r"TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>&",
                                r"L1Analysis::L1AnalysisL1UpgradeDataFormat*")
  path = os.path.dirname(options.output)
  path += "menulib.hh"
  with open(path, "wb") as fp:
    fp.write(text)


  path = os.path.dirname(options.output)
  path += "menu.txt"
  fp = open(path, "wb")

  header = '''#============================================================================#          
#-------------------------------     Menu     -------------------------------#          
#============================================================================#          
# L1Seed                                                     Bit  Prescale POG     PAG 
'''
  fp.write(header)
  algorithms = menu.getAlgorithmMapPtr()
  for name, algorithm in algorithms.iteritems():
    fp.write('{:60} {:>3}         1\n'.format(name, algorithm.getIndex()))
  fp.close()
# eof
