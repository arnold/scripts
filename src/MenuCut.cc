#include "menulib.hh"
#include "MenuObject.hh"
#include "MenuCut.hh"

namespace MenuCut
{

template<class RandomIt, class T> RandomIt
find_index(RandomIt left,
           RandomIt right,
           const T& value,
           bool getRight)
{
  bool isEqual = (std::abs(value - *left) <= 2.*std::numeric_limits<double>::epsilon());
  if (isEqual) return left;

  auto distance = std::distance(left, right);
  if (distance == 1)
  {
    isEqual = (std::abs(value - *left) <= 2.*std::numeric_limits<double>::epsilon());
    if (isEqual) return left;
    if (getRight) return right;
    else return left;
  }

  auto centre = (left + (distance/2));
  if (value < *centre) return find_index(left, centre, value, getRight);

  return find_index(centre, right, value, getRight);
}


size_t
findIndex(const std::vector<double>& array,
          const double value,
          size_t* rawIndex,
          bool getRight)
{
  auto it = find_index(array.begin(), array.end()-1, value, getRight);
  size_t index = std::distance(array.begin(), it);
  if (rawIndex) *rawIndex = index;
  if ((*(array.begin()) == 0.) or (*(array.begin()) == -0.5)) return index; // et array

  auto centre = find_index(array.begin(), array.end()-1, 0.);
  size_t offset = std::distance(array.begin(), centre);

  return index - offset;
}


int
getPrecision(const MenuObject& obj1, const MenuObject& obj2, const int key)
{
  int rc = 0;
  if (obj1.type == obj2.type)
  {
    if (obj1.type == MenuObject::Egamma)
    {
      // EG-EG
      if (key == Delta) rc = prec_eg_eg_delta;
      else if (key == Mass) rc = prec_eg_eg_mass;
      else if (key == MassPt) rc = prec_eg_eg_massPt;
      else if (key == Math) rc = prec_eg_eg_math;
    }
    else if (obj1.type == MenuObject::Jet)
    {
      // JET-JET
      if (key == Delta) rc = prec_jet_jet_delta;
      else if (key == Mass) rc = prec_jet_jet_mass;
      else if (key == MassPt) rc = prec_jet_jet_massPt;
      else if (key == Math) rc = prec_jet_jet_math;
    }
    else if (obj1.type == MenuObject::Tau)
    {
      // TAU-TAU
      if (key == Delta) rc = prec_tau_tau_delta;
      else if (key == Mass) rc = prec_tau_tau_mass;
      else if (key == MassPt) rc = prec_tau_tau_massPt;
      else if (key == Math) rc = prec_tau_tau_math;
    }
    else if (obj1.type == MenuObject::Muon)
    {
      // MU-MU
      if (key == Delta) rc = prec_mu_mu_delta;
      else if (key == Mass) rc = prec_mu_mu_mass;
      else if (key == MassPt) rc = prec_mu_mu_massPt;
      else if (key == Math) rc = prec_mu_mu_math;
    }
    else
    {
      throw std::runtime_error("MenuCut::getPrecision: unhandled - same type");
    }
  }
  else
  {
    if (obj1.type == MenuObject::Egamma)
    {
      if (obj2.type == MenuObject::Jet)
      {
        // EG-JET
        if (key == Delta) rc = prec_eg_jet_delta;
        else if (key == Mass) rc = prec_eg_jet_mass;
        else if (key == MassPt) rc = prec_eg_jet_massPt;
        else if (key == Math) rc = prec_eg_jet_math;
      }
      else if (obj2.type == MenuObject::Tau)
      {
        // EG-TAU
        if (key == Delta) rc = prec_eg_tau_delta;
        else if (key == Mass) rc = prec_eg_tau_mass;
        else if (key == MassPt) rc = prec_eg_tau_massPt;
        else if (key == Math) rc = prec_eg_tau_math;
      }
      else if (obj2.type == MenuObject::Muon)
      {
        // EG-MU
        if (key == Delta) rc = prec_eg_mu_delta;
        else if (key == Mass) rc = prec_eg_mu_mass;
        else if (key == MassPt) rc = prec_eg_mu_massPt;
        else if (key == Math) rc = prec_eg_mu_math;
      }
      else if (obj2.type == L1Analysis::kMissingEt)
      {
        // EG-ETM
        if (key == Delta) rc = prec_eg_etm_delta;
      }
      else if (obj2.type == L1Analysis::kMissingHt)
      {
        // EG-HTM
        if (key == Delta) rc = prec_eg_htm_delta;
      }
#ifdef HAS_ETMHF_SCALE
      else if (obj2.type == L1Analysis::kMissingEtHF)
      {
        // EG-ETMHF
        if (key == Delta) rc = prec_eg_etmhf_delta;
      }
#endif
      else
      {
        throw std::runtime_error("MenuCut::getPrecision: unhandled - EG");
      }
    }
    else if (obj1.type == MenuObject::Jet)
    {
      if (obj2.type == MenuObject::Egamma)
      {
        // EG-JET
        if (key == Delta) rc = prec_eg_jet_delta;
        else if (key == Mass) rc = prec_eg_jet_mass;
        else if (key == MassPt) rc = prec_eg_jet_massPt;
        else if (key == Math) rc = prec_eg_jet_math;
      }
      else if (obj2.type == MenuObject::Tau)
      {
        // JET-TAU
        if (key == Delta) rc = prec_jet_tau_delta;
        else if (key == Mass) rc = prec_jet_tau_mass;
        else if (key == MassPt) rc = prec_jet_tau_massPt;
        else if (key == Math) rc = prec_jet_tau_math;
      }
      else if (obj2.type == MenuObject::Muon)
      {
        // JET-MU
        if (key == Delta) rc = prec_jet_mu_delta;
        else if (key == Mass) rc = prec_jet_mu_mass;
        else if (key == MassPt) rc = prec_jet_mu_massPt;
        else if (key == Math) rc = prec_jet_mu_math;
      }
      else if (obj2.type == L1Analysis::kMissingEt)
      {
        // JET-ETM
        if (key == Delta) rc = prec_jet_etm_delta;
      }
      else if (obj2.type == L1Analysis::kMissingHt)
      {
        // JET-HTM
        if (key == Delta) rc = prec_jet_htm_delta;
      }
#ifdef HAS_ETMHF_SCALE
      else if (obj2.type == L1Analysis::kMissingEtHF)
      {
        // JET-ETMHF
        if (key == Delta) rc = prec_jet_etmhf_delta;
      }
#endif
      else
      {
        throw std::runtime_error("MenuCut::getPrecision: unhandled - JET");
      }
    }
    else if (obj1.type == MenuObject::Tau)
    {
      if (obj2.type == MenuObject::Egamma)
      {
        // EG-TAU
        if (key == Delta) rc = prec_eg_tau_delta;
        else if (key == Mass) rc = prec_eg_tau_mass;
        else if (key == MassPt) rc = prec_eg_tau_massPt;
        else if (key == Math) rc = prec_eg_tau_math;
      }
      else if (obj2.type == MenuObject::Jet)
      {
        // JET-TAU
        if (key == Delta) rc = prec_jet_tau_delta;
        else if (key == Mass) rc = prec_jet_tau_mass;
        else if (key == MassPt) rc = prec_jet_tau_massPt;
        else if (key == Math) rc = prec_jet_tau_math;
      }
      else if (obj2.type == MenuObject::Muon)
      {
        // TAU-MU
        if (key == Delta) rc = prec_tau_mu_delta;
        else if (key == Mass) rc = prec_tau_mu_mass;
        else if (key == MassPt) rc = prec_tau_mu_massPt;
        else if (key == Math) rc = prec_tau_mu_math;
      }
      else if (obj2.type == L1Analysis::kMissingEt)
      {
        // TAU-ETM
        if (key == Delta) rc = prec_tau_etm_delta;
      }
      else if (obj2.type == L1Analysis::kMissingHt)
      {
        // TAU-HTM
        if (key == Delta) rc = prec_tau_htm_delta;
      }
#ifdef HAS_ETMHF_SCALE
      else if (obj2.type == L1Analysis::kMissingEtHF)
      {
        // TAU-ETMHF
        if (key == Delta) rc = prec_tau_etmhf_delta;
      }
#endif
      else
      {
        throw std::runtime_error("MenuCut::getPrecision: unhandled - JET");
      }
    }
    else if (obj1.type == MenuObject::Muon)
    {
      if (obj2.type == MenuObject::Egamma)
      {
        // EG-MU
        if (key == Delta) rc = prec_eg_mu_delta;
        else if (key == Mass) rc = prec_eg_mu_mass;
        else if (key == MassPt) rc = prec_eg_mu_massPt;
        else if (key == Math) rc = prec_eg_mu_math;
      }
      else if (obj2.type == MenuObject::Jet)
      {
        // JET-MU
        if (key == Delta) rc = prec_jet_mu_delta;
        else if (key == Mass) rc = prec_jet_mu_mass;
        else if (key == MassPt) rc = prec_jet_mu_massPt;
        else if (key == Math) rc = prec_jet_mu_math;
      }
      else if (obj2.type == MenuObject::Tau)
      {
        // TAU-MU
        if (key == Delta) rc = prec_tau_mu_delta;
        else if (key == Mass) rc = prec_tau_mu_mass;
        else if (key == MassPt) rc = prec_tau_mu_massPt;
        else if (key == Math) rc = prec_tau_mu_math;
      }
      else if (obj2.type == L1Analysis::kMissingEt)
      {
        // MU-ETM
        if (key == Delta) rc = prec_mu_etm_delta;
      }
      else if (obj2.type == L1Analysis::kMissingHt)
      {
        // MU-HTM
        if (key == Delta) rc = prec_mu_htm_delta;
      }
#ifdef HAS_ETMHF_SCALE
      else if (obj2.type == L1Analysis::kMissingEtHF)
      {
        // MU-ETMHF
        if (key == Delta) rc = prec_mu_etmhf_delta;
      }
#endif
      else
      {
        std::stringstream ss;
        ss << "MenuCut::getPrecision: unhandled - MU: " << obj2.type;
        throw std::runtime_error(ss.str());
      }
    }
    else if (obj1.type == L1Analysis::kMissingEt)
    {
      if (obj2.type == MenuObject::Egamma)
      {
        // EG-ETM
        if (key == Delta) rc = prec_eg_etm_delta;
      }
      else if (obj2.type == MenuObject::Jet)
      {
        // JET-ETM
        if (key == Delta) rc = prec_jet_etm_delta;
      }
      else if (obj2.type == MenuObject::Tau)
      {
        // TAU-ETM
        if (key == Delta) rc = prec_tau_etm_delta;
      }
      else if (obj2.type == MenuObject::Muon)
      {
        // MU-ETM
        if (key == Delta) rc = prec_mu_etm_delta;
      }
      else
      {
        throw std::runtime_error("MenuCut::getPrecision: unhandled - ETM");
      }
    }
    else if (obj1.type == L1Analysis::kMissingHt)
    {
      if (obj2.type == MenuObject::Egamma)
      {
        // EG-HTM
        if (key == Delta) rc = prec_eg_htm_delta;
      }
      else if (obj2.type == MenuObject::Jet)
      {
        // JET-HTM
        if (key == Delta) rc = prec_jet_htm_delta;
      }
      else if (obj2.type == MenuObject::Tau)
      {
        // TAU-HTM
        if (key == Delta) rc = prec_tau_htm_delta;
      }
      else if (obj2.type == MenuObject::Muon)
      {
        // MU-HTM
        if (key == Delta) rc = prec_mu_htm_delta;
      }
      else
      {
        throw std::runtime_error("MenuCut::getPrecision: unhandled - HTM");
      }
    }
#ifdef HAS_ETMHF_SCALE
    else if (obj1.type == L1Analysis::kMissingEtHF)
    {
      if (obj2.type == MenuObject::Egamma)
      {
        // EG-ETMHF
        if (key == Delta) rc = prec_eg_etmhf_delta;
      }
      else if (obj2.type == MenuObject::Jet)
      {
        // JET-ETMHF
        if (key == Delta) rc = prec_jet_etmhf_delta;
      }
      else if (obj2.type == MenuObject::Tau)
      {
        // TAU-ETMHF
        if (key == Delta) rc = prec_tau_etmhf_delta;
      }
      else if (obj2.type == MenuObject::Muon)
      {
        // MU-ETMHF
        if (key == Delta) rc = prec_mu_etmhf_delta;
      }
      else
      {
        throw std::runtime_error("MenuCut::getPrecision: unhandled - ETMHF");
      }
    }
#endif
    else
    {
      throw std::runtime_error("MenuCut::getPrecision: unhandled - diff type");
    }
  }

  return rc;
}


void
setConvLuts(const int type,
            const int* &lut_conv,
            const int* &lut_delta,
            int& nn,
            bool isEta)
{
  if (type == MenuObject::Egamma)
  {
    nn = isEta ? eg_eta_bits : mu_phi_ipi;
    lut_conv = isEta ? LUT_ETA_EG2MU : LUT_PHI_EG2MU;
    lut_delta = isEta ? LUT_DETA_EG_MU : LUT_DPHI_EG_MU;
  }
  else if (type == MenuObject::Jet)
  {
    nn = isEta ? jet_eta_bits : mu_phi_ipi;
    lut_conv = isEta ? LUT_ETA_JET2MU : LUT_PHI_JET2MU;
    lut_delta = isEta ? LUT_DETA_JET_MU : LUT_DPHI_JET_MU;
  }
  else if (type == MenuObject::Tau)
  {
    nn = isEta ? tau_eta_bits : mu_phi_ipi;
    lut_conv = isEta ? LUT_ETA_TAU2MU : LUT_PHI_TAU2MU;
    lut_delta = isEta ? LUT_DETA_TAU_MU : LUT_DETA_TAU_MU;
  }
  else if (type == L1Analysis::kMissingEt)
  {
    nn = etm_phi_ipi;
    lut_conv = LUT_PHI_ETM2MU;
  }
  else if (type == L1Analysis::kMissingHt)
  {
    nn = htm_phi_ipi;
    lut_conv = LUT_PHI_HTM2MU;
  }
#ifdef HAS_ETMHF_SCALE
  else if (type == L1Analysis::kMissingEtHF)
  {
    nn = etmhf_phi_ipi;
    lut_conv = LUT_PHI_ETMHF2MU;
  }
#endif
  else
  {
    throw std::runtime_error("MenuCut::setConvLuts: unhandled type");
  }
}


long long
getDeltaEta(const MenuObject& obj0,
            const MenuObject& obj1,
            const int type)
{
  int delta = 0;
  const int *lut_delta = 0;
  const int *lut_cosh = 0;

  if (obj0.type == obj1.type) // same object type
  {
    if (obj0.type == MenuObject::Muon)
    {
      lut_delta = LUT_DETA_MU_MU;
      lut_cosh = LUT_COSH_DETA_MU_MU;
    }
    else if (obj0.type == MenuObject::Egamma)
    {
      lut_delta = LUT_DETA_EG_EG;
      lut_cosh = LUT_COSH_DETA_EG_EG;
    }
    else if (obj0.type == MenuObject::Jet)
    {
      lut_delta = LUT_DETA_JET_JET;
      lut_cosh = LUT_COSH_DETA_JET_JET;
    }
    else if (obj0.type == MenuObject::Tau)
    {
      lut_delta = LUT_DETA_TAU_TAU;
      lut_cosh = LUT_COSH_DETA_TAU_TAU;
    }
    else
    {
      throw std::runtime_error("MenuCut::getDeltaEta: unhandled - same type");
    }

    delta = abs(obj0.ieta - obj1.ieta);
  }
  else  // different object type
  {
    // one leg is muon
    if ((obj0.type == MenuObject::Muon) or (obj1.type == MenuObject::Muon))
    {
      const MenuObject* muon;
      const MenuObject* calo;
      if (obj0.type == MenuObject::Muon)
      {
        muon = &obj0;
        calo = &obj1;
      }
      else if (obj1.type == MenuObject::Muon)
      {
        muon = &obj1;
        calo = &obj0;
      }

      const int *lut_conv = 0;
      int bits = 0;
      setConvLuts(calo->type, lut_conv, lut_delta, bits, true);

      int ieta = calo->ieta;
      if (ieta < 0) ieta += (1 << bits);
      ieta = lut_conv[ieta];
      delta = abs(ieta - muon->ieta);

      if (calo->type == MenuObject::Egamma) lut_cosh = LUT_COSH_DETA_EG_MU;
      else if (calo->type == MenuObject::Jet) lut_cosh = LUT_COSH_DETA_JET_MU;
      else if (calo->type == MenuObject::Tau) lut_cosh = LUT_COSH_DETA_TAU_MU;
      else
      {
        throw std::runtime_error("MenuCut::getDeltaEta: unhandled - MU");
      }
    }
    // calo-calo
    else
    {
      if (obj0.type == MenuObject::Egamma)
      {
        if (obj1.type == MenuObject::Jet)
        {
          lut_delta = LUT_DETA_EG_JET;
          lut_cosh = LUT_COSH_DETA_EG_JET;
        }
        else if (obj1.type == MenuObject::Tau)
        {
          lut_delta = LUT_DETA_EG_TAU;
          lut_cosh = LUT_COSH_DETA_EG_TAU;
        }
        else
        {
          throw std::runtime_error("MenuCut::getDeltaEta: unhandled - EG");
        }
      }
      else if (obj0.type == MenuObject::Jet)
      {
        if (obj1.type == MenuObject::Egamma)
        {
          lut_delta = LUT_DETA_EG_JET;
          lut_cosh = LUT_COSH_DETA_EG_JET;
        }
        else if (obj1.type == MenuObject::Tau)
        {
          lut_delta = LUT_DETA_JET_TAU;
          lut_cosh = LUT_COSH_DETA_JET_TAU;
        }
        else
        {
          throw std::runtime_error("MenuCut::getDeltaEta: unhandled - JET");
        }
      }
      else if (obj0.type == MenuObject::Tau)
      {
        if (obj1.type == MenuObject::Egamma)
        {
          lut_delta = LUT_DETA_EG_TAU;
          lut_cosh = LUT_COSH_DETA_EG_TAU;
        }
        else if (obj1.type == MenuObject::Jet)
        {
          lut_delta = LUT_DETA_JET_TAU;
          lut_cosh = LUT_COSH_DETA_JET_TAU;
        }
        else
        {
          throw std::runtime_error("MenuCut::getDeltaEta: unhandled - TAU");
        }
      }
      else
      {
        throw std::runtime_error("MenuCut::getDeltaEta: unhandled - calo");
      }
      delta = abs(obj0.ieta - obj1.ieta);
    }
  }


  long long rc = 0;
  if (type == DeltaEta)
  {
    rc = lut_delta[delta];
  }
  else if (type == CoshDeltaEta)
  {
    rc = lut_cosh[delta];
  }
  else
  {
    throw std::runtime_error("MenuCut::getDeltaEta: unhandled - delta");
  }

  return rc;
}


long long
getDeltaPhi(const MenuObject& obj0,
            const MenuObject& obj1,
            const int type)
{
  int ipi = 0;
  int delta = 0;
  const int *lut_delta = 0;
  const int *lut_cos = 0;

  if (obj0.type == obj1.type) // same object type
  {
    if (obj0.type == MenuObject::Muon)
    {
      ipi = mu_phi_ipi;
      lut_delta = LUT_DPHI_MU_MU;
      lut_cos = LUT_COS_DPHI_MU_MU;
    }
    else if (obj0.type == MenuObject::Egamma)
    {
      ipi = eg_phi_ipi;
      lut_delta = LUT_DPHI_EG_EG;
      lut_cos = LUT_COS_DPHI_EG_EG;
    }
    else if (obj0.type == MenuObject::Jet)
    {
      ipi = jet_phi_ipi;
      lut_delta = LUT_DPHI_JET_JET;
      lut_cos = LUT_COS_DPHI_JET_JET;
    }
    else if (obj0.type == MenuObject::Tau)
    {
      ipi = tau_phi_ipi;
      lut_delta = LUT_DPHI_TAU_TAU;
      lut_cos = LUT_COS_DPHI_TAU_TAU;
    }
    else
    {
      throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - same type");
    }
    delta = abs(obj0.iphi - obj1.iphi);
  }
  else  // different object type
  {
    // one leg is muon
    if ((obj0.type == MenuObject::Muon) or (obj1.type == MenuObject::Muon))
    {
      const MenuObject* muon;
      const MenuObject* calo;
      if (obj0.type == MenuObject::Muon)
      {
        muon = &obj0;
        calo = &obj1;
      }
      else if (obj1.type == MenuObject::Muon)
      {
        muon = &obj1;
        calo = &obj0;
      }

      const int *lut_conv = 0;
      setConvLuts(calo->type, lut_conv, lut_delta, ipi, false);

      int iphi = calo->iphi;
      iphi = lut_conv[iphi];
      delta = abs(iphi - muon->iphi);
      if (calo->type == MenuObject::Egamma) lut_cos = LUT_COS_DPHI_EG_MU;
      else if (calo->type == MenuObject::Jet) lut_cos = LUT_COS_DPHI_JET_MU;
      else if (calo->type == MenuObject::Tau) lut_cos = LUT_COS_DPHI_TAU_MU;
      else
      {
        throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - MU");
      }
    }
    // calo-calo
    else
    {
      if (obj0.type == MenuObject::Egamma)
      {
        ipi = eg_phi_ipi;
        if (obj1.type == MenuObject::Jet)
        {
          lut_delta = LUT_DPHI_EG_JET;
          lut_cos = LUT_COS_DPHI_EG_JET;
        }
        else if (obj1.type == MenuObject::Tau)
        {
          lut_delta = LUT_DPHI_EG_TAU;
          lut_cos = LUT_COS_DPHI_EG_TAU;
        }
        else if (type == L1Analysis::kMissingEt) lut_delta = LUT_DPHI_EG_ETM;
        else if (type == L1Analysis::kMissingHt) lut_delta = LUT_DPHI_EG_HTM;
#ifdef HAS_ETMHF_SCALE
        else if (type == L1Analysis::kMissingEtHF) lut_delta = LUT_DPHI_EG_ETMHF;
#endif
        else
        {
          throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - EG");
        }
      }
      else if (obj0.type == MenuObject::Jet)
      {
        ipi = jet_phi_ipi;
        if (obj1.type == MenuObject::Egamma)
        {
          lut_delta = LUT_DPHI_EG_JET;
          lut_cos = LUT_COS_DPHI_EG_JET;
        }
        else if (obj1.type == MenuObject::Tau)
        {
          lut_delta = LUT_DPHI_JET_TAU;
          lut_cos = LUT_COS_DPHI_JET_TAU;
        }
        else if (type == L1Analysis::kMissingEt) lut_delta = LUT_DPHI_JET_ETM;
        else if (type == L1Analysis::kMissingHt) lut_delta = LUT_DPHI_JET_HTM;
#ifdef HAS_ETMHF_SCALE
        else if (type == L1Analysis::kMissingEtHF) lut_delta = LUT_DPHI_JET_ETMHF;
#endif
        else
        {
          throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - JET");
        }
      }
      else if (obj0.type == MenuObject::Tau)
      {
        ipi = tau_phi_ipi;
        if (obj1.type == MenuObject::Egamma)
        {
          lut_delta = LUT_DPHI_EG_TAU;
          lut_cos = LUT_COS_DPHI_EG_TAU;
        }
        else if (obj1.type == MenuObject::Jet)
        {
          lut_delta = LUT_DPHI_JET_TAU;
          lut_cos = LUT_COS_DPHI_JET_TAU;
        }
        else if (type == L1Analysis::kMissingEt) lut_delta = LUT_DPHI_TAU_ETM;
        else if (type == L1Analysis::kMissingHt) lut_delta = LUT_DPHI_TAU_HTM;
#ifdef HAS_ETMHF_SCALE
        else if (type == L1Analysis::kMissingEtHF) lut_delta = LUT_DPHI_TAU_ETMHF;
#endif
        else
        {
          throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - TAU");
        }
      }
      else if (obj0.type == L1Analysis::kMissingEt)
      {
        if (obj1.type == MenuObject::Egamma)
        {
          ipi = eg_phi_ipi;
          lut_delta = LUT_DPHI_EG_ETM;
        }
        else if (obj1.type == MenuObject::Jet)
        {
          ipi = jet_phi_ipi;
          lut_delta = LUT_DPHI_JET_ETM;
        }
        else if (obj1.type == MenuObject::Tau)
        {
          ipi = tau_phi_ipi;
          lut_delta = LUT_DPHI_TAU_ETM;
        }
        else
        {
          throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - ETM");
        }
      }
      else if (obj0.type == L1Analysis::kMissingHt)
      {
        if (obj1.type == MenuObject::Egamma)
        {
          ipi = eg_phi_ipi;
          lut_delta = LUT_DPHI_EG_HTM;
        }
        else if (obj1.type == MenuObject::Jet)
        {
          ipi = jet_phi_ipi;
          lut_delta = LUT_DPHI_JET_HTM;
        }
        else if (obj1.type == MenuObject::Tau)
        {
          ipi = tau_phi_ipi;
          lut_delta = LUT_DPHI_TAU_HTM;
        }
        else
        {
          throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - HTM");
        }
      }
#ifdef HAS_ETMHF_SCALE
      else if (obj0.type == L1Analysis::kMissingEtHF)
      {
        if (obj1.type == MenuObject::Egamma)
        {
          ipi = eg_phi_ipi;
          lut_delta = LUT_DPHI_EG_ETMHF;
        }
        else if (obj1.type == MenuObject::Jet)
        {
          ipi = jet_phi_ipi;
          lut_delta = LUT_DPHI_JET_ETMHF;
        }
        else if (obj1.type == MenuObject::Tau)
        {
          ipi = tau_phi_ipi;
          lut_delta = LUT_DPHI_TAU_ETMHF;
        }
        else
        {
          throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - ETMHF");
        }
      }
#endif
      else
      {
        throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - calo");
      }
      delta = abs(obj0.iphi - obj1.iphi);
    }
  }

  if (delta >= ipi) delta = 2*ipi - delta;

  long long rc = 0;
  if (type == DeltaPhi)
  {
    rc = lut_delta[delta];
  }
  else if (type == CosDeltaPhi)
  {
    rc = lut_cos[delta];
  }
  else
  {
    throw std::runtime_error("MenuCut::getDeltaPhi: unhandled - delta");
  }

  return rc;
}


int
getEt(const MenuObject& object)
{
  const int* array = 0;
  if (object.type == MenuObject::Muon) array = LUT_MU_ET;
  else if (object.type == MenuObject::Egamma) array = LUT_EG_ET;
  else if (object.type == MenuObject::Jet) array = LUT_JET_ET;
  else if (object.type == MenuObject::Tau) array = LUT_TAU_ET;
  else
  {
    throw std::runtime_error("MenuCut::getEt: unknown type");
  }

  return array[object.iet];
}


void
Object::clear()
{
  version = MenuCut::ObjectVersion;
  type = MenuObject::Undef;
  bx = 0;
  threshold = 0.;
  eta1.clear();
  eta2.clear();
  phi1.clear();
  phi2.clear();
  quality = NO_QUALITY;
  isolation = NO_ISOLATION;
  charge = 0;
  ithreshold = -1;
  ieta1.clear();
  ieta2.clear();
  iphi1.clear();
  iphi2.clear();
}


void
Object::setEmulationCut()
{
  if (not (ithreshold < 0)) return;

  const std::vector<double>* et = 0;
  const std::vector<double>* eta_min = 0;
  const std::vector<double>* eta_max = 0;
  const std::vector<double>* phi_min = 0;
  const std::vector<double>* phi_max = 0;

  if (type == MenuObject::Muon)
  {
    et = &MU_ET_MIN;
    eta_min = &MU_ETA_MIN;
    eta_max = &MU_ETA_MAX;
    phi_min = &MU_PHI_MIN;
    phi_max = &MU_PHI_MAX;
  }
  else if (type == MenuObject::Egamma)
  {
    et = &EG_ET_MIN;
    eta_min = &EG_ETA_MIN;
    eta_max = &EG_ETA_MAX;
    phi_min = &EG_PHI_MIN;
    phi_max = &EG_PHI_MAX;
  }
  else if (type == MenuObject::Jet)
  {
    et = &JET_ET_MIN;
    eta_min = &JET_ETA_MIN;
    eta_max = &JET_ETA_MAX;
    phi_min = &JET_PHI_MIN;
    phi_max = &JET_PHI_MAX;
  }
  else if (type == MenuObject::Tau)
  {
    et = &TAU_ET_MIN;
    eta_min = &TAU_ETA_MIN;
    eta_max = &TAU_ETA_MAX;
    phi_min = &TAU_PHI_MIN;
    phi_max = &TAU_PHI_MAX;
  }
  else if (type == L1Analysis::kTotalEt)
  {
    et = &ETT_ET_MIN;
  }
  else if (type == L1Analysis::kTotalHt)
  {
    et = &HTT_ET_MIN;
  }
  else if (type == L1Analysis::kMissingEt)
  {
    et = &ETM_ET_MIN;
    phi_min = &ETM_PHI_MIN;
    phi_max = &ETM_PHI_MAX;
  }
  else if (type == L1Analysis::kMissingHt)
  {
    et = &HTM_ET_MIN;
    phi_min = &HTM_PHI_MIN;
    phi_max = &HTM_PHI_MAX;
  }
#ifdef HAS_MINBIAS_HF_SCALE
  else if (type == L1Analysis::kMinBiasHFP0)
  {
    et = &MBT0HFP_COUNT_MIN;
  }
  else if (type == L1Analysis::kMinBiasHFM0)
  {
    et = &MBT0HFM_COUNT_MIN;
  }
  else if (type == L1Analysis::kMinBiasHFP1)
  {
    et = &MBT1HFP_COUNT_MIN;
  }
  else if (type == L1Analysis::kMinBiasHFM1)
  {
    et = &MBT1HFM_COUNT_MIN;
  }
#endif
#ifdef HAS_ETTEM_SCALE
  else if (type == L1Analysis::kTotalEtEm)
  {
    et = &ETTEM_ET_MIN;
  }
#endif
#ifdef HAS_ETMHF_SCALE
  else if (type == L1Analysis::kMissingEtHF)
  {
    et = &ETMHF_ET_MIN;
    phi_min = &ETMHF_PHI_MIN;
    phi_max = &ETMHF_PHI_MAX;
  }
#endif
#ifdef HAS_TOWERCOUNT_SCALE
  else if (type == L1Analysis::kTowerCount)
  {
    et = &TOWERCOUNT_COUNT_MIN;
  }
#endif
  else
  {
    throw std::runtime_error("ObjectCut::setEmulationCut: unknown type");
  }

  size_t index = 0;
  const bool isMax = true;
  ithreshold = findIndex(*et, threshold, &index);
  threshold = et->at(index);

  if (eta1.minimum != eta1.maximum)
  {
    ieta1.minimum = findIndex(*eta_min, eta1.minimum, &index);
    eta1.minimum = eta_min->at(index);

    ieta1.maximum = findIndex(*eta_max, eta1.maximum, &index, isMax);
    eta1.maximum = eta_max->at(index);

    if (eta2.minimum != eta2.maximum)
    {
      ieta2.minimum = findIndex(*eta_min, eta2.minimum, &index);
      eta2.minimum = eta_min->at(index);

      ieta2.maximum = findIndex(*eta_max, eta2.maximum, &index, isMax);
      eta2.maximum = eta_max->at(index);
    }
  }

  if (phi1.minimum != phi1.maximum)
  {
    iphi1.minimum = findIndex(*phi_min, phi1.minimum, &index);
    phi1.minimum = phi_min->at(index);
    iphi1.maximum = findIndex(*phi_max, phi1.maximum, &index, isMax);
    phi1.maximum = phi_max->at(index);

    if (phi2.minimum != phi2.maximum)
    {
      iphi2.minimum = findIndex(*phi_min, phi2.minimum, &index);
      phi2.minimum = phi_min->at(index);
      iphi2.maximum = findIndex(*phi_max, phi2.maximum, &index, isMax);
      phi2.maximum = phi_max->at(index);
    }
  }
}


void
Correlation::clear()
{
  version = MenuCut::CorrelationVersion;
  deltaEta.clear();
  deltaPhi.clear();
  deltaR.clear();
  mass.clear();
  charge_correlation = 0;
}


bool
Correlation::select(const MenuObject& obj0,
                    const MenuObject& obj1) const
{
  if (deltaEta.minimum != deltaEta.maximum)
  {
    const int precision = getPrecision(obj0, obj1, Delta);
    const long long minimum = (long long)(deltaEta.minimum * POW10[precision]);
    const long long maximum = (long long)(deltaEta.maximum * POW10[precision]);
    const long long delta = getDeltaEta(obj0, obj1, DeltaEta);
    if (not ((minimum <= delta) and (delta <= maximum))) return false;
  }

  if (deltaPhi.minimum != deltaPhi.maximum)
  {
    const int precision = getPrecision(obj0, obj1, Delta);
    const long long minimum = (long long)(deltaPhi.minimum * POW10[precision]);
    const long long maximum = (long long)(deltaPhi.maximum * POW10[precision]);
    const long long delta = getDeltaPhi(obj0, obj1, DeltaPhi);
    if (not ((minimum <= delta) and (delta <= maximum))) return false;
  }

  if (deltaR.minimum != deltaR.maximum)
  {
    const int precision = getPrecision(obj0, obj1, Delta);
    double x = 0.;
    x = deltaR.minimum;
    x = floor(x*x*POW10[precision])/POW10[precision];
    const long long minimum = (long long)(x * POW10[2*precision]);

    x = deltaR.maximum;
    x = ceil(x*x*POW10[precision])/POW10[precision];
    const long long maximum = (long long)(x * POW10[2*precision]);

    const long long deltaEta = getDeltaEta(obj0, obj1, DeltaEta);
    const long long deltaPhi = getDeltaPhi(obj0, obj1, DeltaPhi);
    const long long deltaR2 = deltaEta*deltaEta + deltaPhi*deltaPhi;
    if (not ((minimum <= deltaR2) and (deltaR2 <= maximum))) return false;
  }
  else if (mass.minimum != mass.maximum)
  {
    const int precision = getPrecision(obj0, obj1, Mass);
    const int shift = 2*getPrecision(obj0, obj1, MassPt) + getPrecision(obj0, obj1, Math);
    double x = 0.;
    x = mass.minimum;
    x = floor(x*x*0.5*POW10[precision])/POW10[precision];
    const long long minimum = (long long)(x * POW10[shift]);

    x = mass.maximum;
    x = floor(x*x*0.5*POW10[precision])/POW10[precision];
    const long long maximum = (long long)(x * POW10[shift]);

    const long long coshDeta = getDeltaEta(obj0, obj1, CoshDeltaEta);
    const long long cosDphi = getDeltaPhi(obj0, obj1, CosDeltaPhi);
    const long long mass2 = getEt(obj0) * getEt(obj1) * (coshDeta - cosDphi);

    if (not ((minimum <= mass2) and (mass2 <= maximum))) return false;
  }

  if (charge_correlation)
  {
    if (not ((obj0.charge * obj1.charge) == charge_correlation)) return false;
  }

  return true;
}
} // namespace MenuCut


void
setCandidates(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data,
              std::vector<int>& candidates,
              const MenuCut::Object& cut)
{
  if (cut.type == MenuObject::Muon)
  {
    for (size_t ii = 0; ii < data->muonBx.size(); ii++)
    {
      if (not (data->muonBx.at(ii) == cut.bx)) continue;
      candidates.push_back(ii);
    }
  }
  else if (cut.type == MenuObject::Egamma)
  {
    for (size_t ii = 0; ii < data->egBx.size(); ii++)
    {
      if (not (data->egBx.at(ii) == cut.bx)) continue;
      candidates.push_back(ii);
    }
  }
  else if (cut.type == MenuObject::Jet)
  {
    for (size_t ii = 0; ii < data->jetBx.size(); ii++)
    {
      if (not (data->jetBx.at(ii) == cut.bx)) continue;
      candidates.push_back(ii);
    }
  }
  else if (cut.type == MenuObject::Tau)
  {
    for (size_t ii = 0; ii < data->tauBx.size(); ii++)
    {
      if (not (data->tauBx.at(ii) == cut.bx)) continue;
      candidates.push_back(ii);
      if (candidates.size() == MenuCut::MaxTaus) break;
    }
  }
  else
  {
    for (size_t ii = 0; ii < data->sumType.size(); ii++)
    {
      if (not (data->sumType.at(ii) == cut.type)) continue;
      if (not (data->sumBx.at(ii) == cut.bx)) continue;
      candidates.push_back(ii);
    }
  }
}

// eof
